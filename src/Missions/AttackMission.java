package Missions;

import AerialVehicleKits.AttackingKit;
import AerialVehicleKits.KitType;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import MssionTools.MissileType;

import static Interactors.UserInteractor.sendAttackingMissionInformation;

public class AttackMission extends Mission {
    private final String target;

    public AttackMission(AerialVehicle aerialVehicle, Coordinates targetCoordinates, String pilotName, String target) {
        super(aerialVehicle, targetCoordinates, pilotName);
        this.target = target;
    }

    @Override
    public void execute() throws AerialVehicleNotCompatibleException {
        AttackingKit attackingKit = (AttackingKit) performingAerialVehicle.getAerialVehicleKits().get(KitType.ATTACKING_KIT);
        if (attackingKit == null)
            throw new AerialVehicleNotCompatibleException("Aerial vehicle " + performingAerialVehicle.getClass().getSimpleName() + " does not support " + this.getClass().getSimpleName());
        performingAerialVehicle.flyTo(targetCoordinates);
        MissileType missileType = attackingKit.getMissileType();
        sendAttackingMissionInformation(pilotName, performingAerialVehicle.getClass().getSimpleName(), target, missileType.toString(), attackingKit.getNumberOfMissiles());
        performingAerialVehicle.land(performingAerialVehicle.getHomeBaseCoordinates());
    }
}
