package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

import static Interactors.UserInteractor.sendBeginningMission;
import static Interactors.UserInteractor.sendCancelingMission;

public abstract class Mission {
    protected final Coordinates targetCoordinates;
    protected String pilotName;
    protected AerialVehicle performingAerialVehicle;

    public Mission(AerialVehicle performingAerialVehicle, Coordinates targetCoordinates, String pilotName) {
        this.performingAerialVehicle = performingAerialVehicle;
        this.targetCoordinates = targetCoordinates;
        this.pilotName = pilotName;
    }

    public void begin() {
        sendBeginningMission();
        performingAerialVehicle.flyTo(targetCoordinates);
    }

    public void cancel() {
        sendCancelingMission();
        performingAerialVehicle.land(performingAerialVehicle.getHomeBaseCoordinates());
    }

    public void finish() throws AerialVehicleNotCompatibleException {
        execute();
        performingAerialVehicle.land(performingAerialVehicle.getHomeBaseCoordinates());
    }

    public abstract void execute() throws AerialVehicleNotCompatibleException;
}
