package Missions;

import AerialVehicleKits.IntelligenceGatheringKit;
import AerialVehicleKits.KitType;
import AerialVehicles.Unmanned.UnmannedAerialVehicle;
import Entities.Coordinates;
import MssionTools.SensorType;

import static Interactors.UserInteractor.sendIntelligenceMissionInformation;

public class IntelligenceMission extends Mission {
    private final String region;

    public IntelligenceMission(UnmannedAerialVehicle aerialVehicle, Coordinates targetCoordinates, String pilotName, String region) {
        super(aerialVehicle, targetCoordinates, pilotName);
        this.region = region;
    }

    @Override
    public void execute() throws AerialVehicleNotCompatibleException {
        IntelligenceGatheringKit intelligenceGatheringKit = (IntelligenceGatheringKit) performingAerialVehicle.getAerialVehicleKits().get(KitType.INTELLIGENCE_GATHERING_KIT);
        if (intelligenceGatheringKit == null)
            throw new AerialVehicleNotCompatibleException("Aerial vehicle " + performingAerialVehicle.getClass().getSimpleName() + " does not support " + this.getClass().getSimpleName());
        performingAerialVehicle.flyTo(targetCoordinates);
        ((UnmannedAerialVehicle) performingAerialVehicle).hoverOverLocation(targetCoordinates);
        SensorType sensorType = intelligenceGatheringKit.getSensorType();
        sendIntelligenceMissionInformation(pilotName, performingAerialVehicle.getClass().getSimpleName(), region, sensorType.toString());
        performingAerialVehicle.land(performingAerialVehicle.getHomeBaseCoordinates());
    }
}
