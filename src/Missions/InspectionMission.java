package Missions;

import AerialVehicleKits.InspectionKit;
import AerialVehicleKits.KitType;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import MssionTools.CameraType;

import static Interactors.UserInteractor.sendInspectionMissionInformation;

public class InspectionMission extends Mission {
    private final String objective;

    public InspectionMission(AerialVehicle performingAerialVehicle, Coordinates targetCoordinates, String pilotName, String objective) {
        super(performingAerialVehicle, targetCoordinates, pilotName);
        this.objective = objective;
    }

    @Override
    public void execute() throws AerialVehicleNotCompatibleException {
        InspectionKit inspectionKit = (InspectionKit) performingAerialVehicle.getAerialVehicleKits().get(KitType.INSPECTION_KIT);
        if (inspectionKit == null)
            throw new AerialVehicleNotCompatibleException("Aerial vehicle " + performingAerialVehicle.getClass().getSimpleName() + " does not support " + this.getClass().getSimpleName());
        performingAerialVehicle.flyTo(targetCoordinates);
        CameraType cameraType = inspectionKit.getCameraType();
        sendInspectionMissionInformation(pilotName, performingAerialVehicle.getClass().getSimpleName(), objective, cameraType.toString());
        performingAerialVehicle.land(performingAerialVehicle.getHomeBaseCoordinates());
    }
}
