package AerialVehicleKits;

import MssionTools.MissileType;

public class AttackingKit implements AerialVehicleKit {
    protected MissileType missileType;
    private int numberOfMissiles;

    public AttackingKit(MissileType missileType, int numberOfMissiles) {
        this.missileType = missileType;
        this.numberOfMissiles = numberOfMissiles;
    }

    public MissileType getMissileType() {
        return missileType;
    }

    public int getNumberOfMissiles() {
        return numberOfMissiles;
    }
}
