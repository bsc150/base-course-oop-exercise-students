package AerialVehicleKits;

import MssionTools.CameraType;

public class InspectionKit implements AerialVehicleKit {
    CameraType cameraType;

    public InspectionKit(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }
}
