package AerialVehicleKits;

public enum KitType {
    ATTACKING_KIT,
    INSPECTION_KIT,
    INTELLIGENCE_GATHERING_KIT
}
