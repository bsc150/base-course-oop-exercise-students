package AerialVehicleKits;

import MssionTools.SensorType;

public class IntelligenceGatheringKit implements AerialVehicleKit {
    private SensorType sensorType;

    public IntelligenceGatheringKit(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public SensorType getSensorType() {
        return sensorType;
    }
}
