package AerialVehicles.Unmanned;

import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

import java.util.Map;

import static Interactors.UserInteractor.sendHoveringOverLocation;
import static Interactors.UserInteractor.sendUnableToHover;

public abstract class UnmannedAerialVehicle extends AerialVehicle {
    public UnmannedAerialVehicle(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }

    public void hoverOverLocation(Coordinates location) {
        if (isReadyForHover())
            sendHoveringOverLocation(location);
        else
            sendUnableToHover();
    }
}
