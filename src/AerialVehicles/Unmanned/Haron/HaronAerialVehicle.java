package AerialVehicles.Unmanned.Haron;

import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.FlightStatus;
import AerialVehicles.Unmanned.UnmannedAerialVehicle;
import Entities.Coordinates;

import java.util.Map;

public abstract class HaronAerialVehicle extends UnmannedAerialVehicle {
    private static final int FLIGHT_HOURS_TILL_MANDATORY_REPAIR = 150;

    public HaronAerialVehicle(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }

    @Override
    protected int getMaxFlightHours() {
        return FLIGHT_HOURS_TILL_MANDATORY_REPAIR;
    }
}
