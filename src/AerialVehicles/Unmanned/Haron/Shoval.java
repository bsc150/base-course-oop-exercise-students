package AerialVehicles.Unmanned.Haron;


import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

import java.util.Map;

public class Shoval extends HaronAerialVehicle {

    public Shoval(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }
}

