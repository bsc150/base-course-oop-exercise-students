package AerialVehicles.Unmanned.Hermes;


import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

import java.util.Map;

public class Zik extends HermesAerialVehicle {

    public Zik(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }
}