package AerialVehicles.Unmanned.Hermes;

import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.FlightStatus;
import AerialVehicles.Unmanned.UnmannedAerialVehicle;
import Entities.Coordinates;

import java.util.Map;

public abstract class HermesAerialVehicle extends UnmannedAerialVehicle {
    private static final int FLIGHT_HOURS_TILL_MANDATORY_REPAIR = 100;

    public HermesAerialVehicle(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }

    @Override
    protected int getMaxFlightHours() {
        return FLIGHT_HOURS_TILL_MANDATORY_REPAIR;
    }
}
