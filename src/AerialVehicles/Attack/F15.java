package AerialVehicles.Attack;


import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

import java.util.Map;

public class F15 extends AttackAerialVehicle {

    public F15(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }
}
