package AerialVehicles.Attack;

import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

import java.util.Map;

public abstract class AttackAerialVehicle extends AerialVehicle {
    private static final int FLIGHT_HOURS_TILL_MANDATORY_REPAIR = 250;

    public AttackAerialVehicle(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates, Map<KitType, AerialVehicleKit> kits) {
        super(flightHoursSinceLastRepair, flightStatus, homeBaseCoordinates, kits);
    }

    @Override
    protected int getMaxFlightHours() {
        return FLIGHT_HOURS_TILL_MANDATORY_REPAIR;
    }
}
