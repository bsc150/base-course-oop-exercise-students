package AerialVehicles;

import AerialVehicleKits.AerialVehicleKit;
import AerialVehicleKits.KitType;
import Entities.Coordinates;

import java.util.Map;

import static Interactors.UserInteractor.*;

public abstract class AerialVehicle {
    private final Coordinates homeBaseCoordinates;
    private final Map<KitType, ? extends AerialVehicleKit> aerialVehicleKits;
    private int flightHoursSinceLastRepair;
    private FlightStatus flightStatus;

    public AerialVehicle(int flightHoursSinceLastRepair, FlightStatus flightStatus, Coordinates homeBaseCoordinates,
                         Map<KitType, ? extends AerialVehicleKit> kits) {
        this.flightHoursSinceLastRepair = flightHoursSinceLastRepair;
        this.flightStatus = flightStatus;
        this.homeBaseCoordinates = homeBaseCoordinates;
        aerialVehicleKits = kits;
    }

    public Map<KitType, ? extends AerialVehicleKit> getAerialVehicleKits() {
        return aerialVehicleKits;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void flyTo(Coordinates destination) {
        if (isReadyForFlight()) {
            sendFlyingTo(destination);
            flightStatus = FlightStatus.IN_AIR;
        } else
            sendUnableToFly();
    }

    public void land(Coordinates destination) {
        if (isInAir()) {
            sendLandingOn(destination);
            checkFlightHoursSinceLastRepair();
        } else {
            sendUnableToLand();
        }
    }

    private boolean isInAir() {
        return flightStatus.equals(FlightStatus.IN_AIR);
    }

    public void checkFlightHoursSinceLastRepair() {
        if (flightHoursSinceLastRepair >= getMaxFlightHours()) {
            flightStatus = FlightStatus.NOT_READY_FOR_FLIGHT;
            repair();
        } else
            flightStatus = FlightStatus.READY_FOR_FLIGHT;
    }

    public void repair() {
        flightHoursSinceLastRepair = 0;
        flightStatus = FlightStatus.READY_FOR_FLIGHT;
    }

    protected abstract int getMaxFlightHours();

    protected boolean isReadyForFlight() {
        return flightStatus.equals(FlightStatus.READY_FOR_FLIGHT);
    }

    protected boolean isReadyForHover() {
        return flightStatus.equals(FlightStatus.IN_AIR);
    }

    public Coordinates getHomeBaseCoordinates() {
        return homeBaseCoordinates;
    }
}
