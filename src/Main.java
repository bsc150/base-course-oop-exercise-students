import AerialVehicleKits.*;
import AerialVehicles.Attack.AttackAerialVehicle;
import AerialVehicles.Attack.F15;
import AerialVehicles.Attack.F16;
import AerialVehicles.FlightStatus;
import AerialVehicles.Unmanned.Haron.Eitan;
import AerialVehicles.Unmanned.UnmannedAerialVehicle;
import Entities.Coordinates;
import Missions.*;
import MssionTools.CameraType;
import MssionTools.MissileType;
import MssionTools.SensorType;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException {
        // Implement scenarios from readme here
        simulateIntelligenceMission();
        simulateAttackMission();
        simulateInspectionMission();
    }

    private static void simulateIntelligenceMission() throws AerialVehicleNotCompatibleException {
        Coordinates coordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        Coordinates homeBaseCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        String pilotName = "Dror zalicha";
        String region = "Iraq";
        Map<KitType, AerialVehicleKit> kits = new HashMap<>();
        kits.put(KitType.INTELLIGENCE_GATHERING_KIT, new IntelligenceGatheringKit(SensorType.INFRA_RED));
        kits.put(KitType.ATTACKING_KIT, new AttackingKit(MissileType.PYTHON, 4));
        UnmannedAerialVehicle eitan = new Eitan(0, FlightStatus.READY_FOR_FLIGHT, homeBaseCoordinates, kits);
        Mission mission = new IntelligenceMission(eitan, coordinates, pilotName, region);
        mission.execute();
    }

    private static void simulateAttackMission() throws AerialVehicleNotCompatibleException {
        Coordinates coordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        Coordinates homeBaseCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        String pilotName = "Ze'ev Raz";
        String target = "Tuwaitha Nuclear Research Center";
        Map<KitType, AerialVehicleKit> kits = new HashMap<>();
        kits.put(KitType.ATTACKING_KIT, new AttackingKit(MissileType.SPICE250, 10));
        kits.put(KitType.INTELLIGENCE_GATHERING_KIT, new IntelligenceGatheringKit(SensorType.INFRA_RED));
        AttackAerialVehicle f15 = new F15(0, FlightStatus.READY_FOR_FLIGHT, homeBaseCoordinates, kits);
        Mission mission = new AttackMission(f15, coordinates, pilotName, target);
        mission.execute();
    }

    private static void simulateInspectionMission() throws AerialVehicleNotCompatibleException {
        Coordinates coordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        Coordinates homeBaseCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        String pilotName = "Ilan Ramon";
        String target = "Tuwaitha Nuclear Research Center";
        Map<KitType, AerialVehicleKit> kits = new HashMap<>();
        kits.put(KitType.ATTACKING_KIT, new AttackingKit(MissileType.PYTHON, 4));
        kits.put(KitType.INSPECTION_KIT, new InspectionKit(CameraType.THERMAL));
        AttackAerialVehicle f16 = new F16(0, FlightStatus.READY_FOR_FLIGHT, homeBaseCoordinates, kits);
        Mission mission = new InspectionMission(f16, coordinates, pilotName, target);
        mission.execute();
    }
}
