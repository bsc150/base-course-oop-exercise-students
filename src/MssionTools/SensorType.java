package MssionTools;

public enum SensorType {
    INFRA_RED("infra red"),
    ELINT("elint");

    private final String sensorName;

    SensorType(String name) {
        this.sensorName = name;
    }

    public String toString() {
        return sensorName;
    }
}
