package MssionTools;

public enum CameraType {
    REGULAR("regular camera"),
    THERMAL("thermal camera"),
    NIGHT_VISION("night vision camera");

    private final String cameraName;

    CameraType(String name) {
        this.cameraName = name;
    }

    public String toString() {
        return cameraName;
    }
}
