package MssionTools;

public enum MissileType {
    PYTHON("python"),
    AMRAM("amram"),
    SPICE250("spice 250");

    private final String missileName;

    MissileType(String name) {
        this.missileName = name;
    }

    public String toString() {
        return missileName;
    }
}
