package Interactors;

import Entities.Coordinates;

public class UserInteractor {
    private UserInteractor(){}

    public static void sendFlyingTo(Coordinates destination){
        System.out.println("Flying to: " + destination.toString());
    }

    public static void sendLandingOn(Coordinates destination){
        System.out.println("Landing on: " + destination.toString());
    }

    public static void sendHoveringOverLocation(Coordinates location){
        System.out.println("Hovering Over: " + location.toString());
    }

    public static void sendUnableToFly(){
        System.out.println("Aerial Vehicle isn't ready to fly");
    }

    public static void sendUnableToLand(){
        System.out.println("Aerial Vehicle isn't ready to land");
    }

    public static void sendUnableToHover() {
        System.out.println("Aerial Vehicle isn't ready to hover");
    }

    public static void sendBeginningMission() {
        System.out.println("Beginning Mission!");
    }

    public static void sendCancelingMission() {
        System.out.println("Canceling Mission!");
    }

    public static void sendIntelligenceMissionInformation(String pilotName, String aerialVehicleName, String region, String sensor) {
        System.out.printf("%s: %s Collecting Data in %s with: sensor type: %s%n", pilotName, aerialVehicleName, region, sensor);
    }

    public static void sendInspectionMissionInformation(String pilotName, String aerialVehicleName, String objective, String cameraTypeName) {
        System.out.printf("%s: %s taking pictures of suspect %s with: %s%n", pilotName, aerialVehicleName, objective, cameraTypeName);
    }

    public static void sendAttackingMissionInformation(String pilotName, String aerialVehicleName, String objective, String missileTypeName, int numberOfMissiles) {
        System.out.printf("%s: %s Attacking suspect %s with: %sx%s%n", pilotName, aerialVehicleName, objective, missileTypeName, numberOfMissiles);
    }
}
